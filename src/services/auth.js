import api from './api'

export const login = function (email, password) {
  return api.post('http://localhost:3000/auth', { username: email, password: password })
}
