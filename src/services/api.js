import axios from 'axios'
import router from '../router'
import store from '../store'

/* ------------------ สร้าง custtom axios ----------------- */
const api = axios.create({
  baseURL: 'http://localhost:3000',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

/* ------------------------- req ------------------------ */
api.interceptors.request.use(
  req => {
    const token = localStorage.getItem('token')
    if (token) {
      req.headers.common.Authorization = 'Bearer ' + token
    }
    return req
  },
  err => {
    return Promise.reject(err)
  }
)
/* ------------------------- res ------------------------ */
api.interceptors.response.use(
  res => {
    if (res.status === 200 || res.status === 201) {
      return Promise.resolve(res)
    } else {
      return Promise.reject(res)
    }
  },
  err => {
    switch (err.response.status) {
      case 400:
        console.log('Status 400')
        break
      case 401:
        console.log('session expired')
        store.dispatch('auth/logout')
        router.replace({
          path: '/login',
          query: { redirect: router.currentRoute.fullPath }
        })
        break
      case 403:
        router.replace({
          path: '/login',
          query: { redirect: router.currentRoute.fullPath }
        })
        break
      case 404:
        alert('page not exist')
        break
      case 502:
        setTimeout(() => {
          router.replace({
            path: '/login',
            query: {
              redirect: router.currentRoute.fullPath
            }
          })
        }, 1000)
        return Promise.reject(err.response)
    }
  }
)

export default api
